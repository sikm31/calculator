import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ua.dp.calculator.model.MainModel;
import ua.dp.calculator.service.MainService;
import ua.dp.calculator.service.MainServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;


/**
 * Created by yuri on 17.06.2016.
 */

public class MainServiceTest {

    @Autowired
    MainService main = new MainServiceImpl();


    @Test
    public void getMainTest(){
        String m1="*";
        Double m2 =3.0;
        Double m3 = 5.0;
        String m4 = "/";
        Double main1 = m2*m3;
        assertEquals(main.OperMulti(m1,m2,m3),main1);
    }

    @Test
    public void getMainTest2(){
        Double m2 =3.0;
        Double m3 = 5.0;
        String m4 = "/";
        Double main2 = m2/m3;
        assertEquals(main.OperMulti(m4,m2,m3),main2);
    }


}
