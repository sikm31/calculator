function doAjaxPost() {
    // get the form values
    var msg   = $('#contactForm1').serialize();

    $.ajax({
        type: "POST",
        url: "/",
        data: msg,
        success: function(response){
            //alert('GOOD ');
            // we have the response
            $('#result').html(response);
        },
        error: function(e){
            alert('Error: ' + e);
        }
    });
}