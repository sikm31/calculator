package ua.dp.calculator.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ua.dp.calculator.service.MainService;

/**
 * Created by yuri on 16.06.2016.
 */
@Controller
@RequestMapping("/")
public class MainController {

    Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    MainService mainService;

    @RequestMapping(method = RequestMethod.GET)
    public String main(){
        return "index";
    }

    @RequestMapping(method = RequestMethod.POST)
    ModelAndView operation(@RequestParam(required = true) Double a,
                     @RequestParam(required = true) String z,
                     @RequestParam(required = true) Double b) {
        logger.info(z);
        return new ModelAndView("index","result",mainService.OperMulti(z,a,b));
    }
}
