package ua.dp.calculator.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.calculator.model.MainModel;

/**
 * Created by yuri on 16.06.2016.
 */
@Service
public class MainServiceImpl implements MainService  {

    Logger logger = LoggerFactory.getLogger(MainServiceImpl.class);


    @Override
    public Double OperMulti(String input,Double a, Double b) throws ArithmeticException{
        double result;
        switch (input){
            case "*": result=a*b;
                break;
            case "/": result=a/b;
                break;
            case "+": result=a+b;
                break;
            case "-": result=a-b;
                break;
            default: result=0;
        }
        return result;
    }
}
