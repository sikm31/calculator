package ua.dp.calculator.model;

import org.springframework.stereotype.Repository;

/**
 * Created by yuri on 16.06.2016.
 */
@Repository
public class MainModel {
    Double a;
    Double b;
    String input;
    Double result;

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public Double getA() {
        return a;
    }

    public void setA(Double a) {
        this.a = a;
    }

    public Double getB() {
        return b;
    }

    public void setB(Double b) {
        this.b = b;
    }
}
